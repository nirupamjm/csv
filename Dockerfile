FROM ubuntu:18.04

MAINTAINER nirupamjm

RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip
RUN apt-get update -y
RUN apt-get install -qqy x11-apps -y
COPY . /app
WORKDIR /app

ENTRYPOINT ["python"]
CMD ["Problem_1.py"]
